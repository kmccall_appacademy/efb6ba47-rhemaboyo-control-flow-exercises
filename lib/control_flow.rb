#require 'byebug'
# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.delete!(str.downcase)
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid = str.length / 2
  if str.length.odd?
    str[mid]
  else
    str[mid - 1] + str[mid]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.count(VOWELS.join)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  if num <= 1
    1
  else
    num * factorial(num - 1)
  end
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  string = ''
  arr.each_index do |i|
    string += arr[i]
    next if i == arr.length - 1
    string += separator
  end
  string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  letters = str.split('')
  letters.each_index do |i|
    if i.odd?
      letters[i] = letters[i].upcase
    else
      letters[i] = letters[i].downcase
    end
  end
  letters.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split(' ')
  words.each_index do |i|
    if words[i].length >= 5
      words[i] = words[i].reverse
    end
  end
  words.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  ints = (1..n).to_a
  ints.each_index do |i|
    if ints[i] % 3 == 0 && ints[i] % 5 == 0
      ints[i] = 'fizzbuzz'
    elsif ints[i] % 3 == 0
      ints[i] = 'fizz'
    elsif ints[i] % 5 == 0
      ints[i] = 'buzz'
    end
  end
  ints
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num <= 1
  (2...num).each do |int|
    if num % int == 0
      return false
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  (1..num).each do |int|
    if num % int == 0
      factors.push(int)
    end
  end
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factors = []
  (1..num).each do |int|
    if num % int == 0 && prime?(int)
      prime_factors.push(int)
    end
  end
  prime_factors
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = []
  odds = []
  arr.each do |int|
    if int.even?
      evens.push(int)
    else
      odds.push(int)
    end
  end
  if evens.length < odds.length
    evens[0]
  else
    odds[0]
  end
end
